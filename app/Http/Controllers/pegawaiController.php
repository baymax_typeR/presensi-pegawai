<?php

namespace App\Http\Controllers;

// use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Redirector;

class pegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    

    public function index()
    {                                                              
        $pegawai = DB::table("pegawai")            
            ->get();

        return view('pegawai', ['pegawai' => $pegawai]);
    }
    
    public function tbpeg(Request $request)
    {      
        $data_array = array(
            'PEGNIK' => $request->PEGNIK,
            'PEGNAMA' => $request->PEGNAMA,
            'PEGKOTA' => $request->PEGKOTA,         
        );

        if ($request->action == "tambah") {
            DB::table('pegawai')->insert($data_array); 
        } else {            
            DB::table('pegawai')->where('PEGNIK', '=', $request->PEGNIK)->update($data_array); 
        }

        $data['message'] = "Sukses!";   
        
        return response()->json($data);
    }   

    public function dlpeg(Request $request, $id)
    {   
        $query = DB::table('pegawai')->where([
                ['PEGNIK', $id],
            ])->delete();

        if ( $query == true ) {
            $data['code'] = "100";
            $data['message'] = "Sukses hapus data pegawai!";
        } else {
            $data['code'] = "404";
            $data['message'] = "Gagal hapus data pegawai!";
        }        
        return response()->json($data);                
    }   
}
