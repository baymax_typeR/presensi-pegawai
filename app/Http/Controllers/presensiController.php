<?php

namespace App\Http\Controllers;

// use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Redirector;

class presensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    

    public function index($id)
    {
        $presensi = DB::table("presensi as b")
            ->select('b.PRESID', 'b.PEGNIK', 'b.PRESTGL', 'b.PRESJAM',
                    'a.PEGNIK', 'a.PEGNAMA')
            ->join('pegawai as a', 'a.PEGNIK', '=', 'b.PEGNIK')
            ->where('b.PEGNIK', '=', $id)
            ->get();

        return view('presensi', ['presensi' => $presensi]);
    }
    
    public function tbpres(Request $request)
    {      
        $data_array = array(
            'PEGNIK' => $request->PEGNIK,
            'PRESTGL' => $request->PRESTGL,
            'PRESJAM' => $request->PRESJAM,         
        );

        DB::table('presensi')->insert($data_array);        

        $data['message'] = "Sukses!";   
        
        return response()->json($data);
    }   

    // public function dlpeg(Request $request, $id)
    // {   
    //     $query = DB::table('pegawai')->where([
    //             ['PEGNIK', $id],
    //         ])->delete();

    //     if ( $query == true ) {
    //         $data['code'] = "100";
    //         $data['message'] = "Sukses hapus data pegawai!";
    //     } else {
    //         $data['code'] = "404";
    //         $data['message'] = "Gagal hapus data pegawai!";
    //     }        
    //     return response()->json($data);                
    // }   
}
