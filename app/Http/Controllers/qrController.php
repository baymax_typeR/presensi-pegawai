<?php

namespace App\Http\Controllers;

// use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Redirector;

class qrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    

    public function index()
    {                                                              
        $pegawai = DB::table("pegawai")            
            ->get();

        return view('buatqr', ['pegawai' => $pegawai]);
    }  

    public function qr($id)
    {                  
        $query = DB::table('pegawai')                                
                ->where('PEGNIK', $id)
                ->get(); 

        $PRESTGL = date('Y-m-d');        
        $PRESJAM = date('H:i:s');        

        $qr['nik'] = $query[0]->PEGNIK;
        $qr['nama'] = $query[0]->PEGNAMA;
        $qr['kota'] = $query[0]->PEGKOTA;                
        $qr['data'] = $query[0]->PEGNIK
                        .'/'.$query[0]->PEGNAMA
                        .'/'.$query[0]->PEGKOTA
                        .'/'.$PRESTGL        
                        .'/'.$PRESJAM;        
        return view('qr', $qr);
    }
}
