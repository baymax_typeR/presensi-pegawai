<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Redirector;

class transaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    

    public function index()
    {                              
        
        $transaksi = DB::table("transaksi as t")
            ->select('t.TRID', 't.CUSTID', 't.TRTGL', 't.PKTID', 't.TRBERAT', 't.TRTOTAL',
                    'c.CUSTNAMA as namacust', 'p.PKTNAMA as namapaket')
            ->join('customer as c', 't.CUSTID', '=', 'c.CUSTID')
            ->join('paket as p', 't.PKTID', '=', 'p.PKTID')            
            ->get(); 
            
        $customer = DB::table("customer")
            ->orderBy('CUSTNAMA')
            ->get();

        $paket = DB::table("paket")
            ->orderBy('PKTNAMA')
            ->get();
        
        return view('transaksi', ['transaksi' => $transaksi, 'customer' => $customer, 'paket' => $paket]);
    }
    
    public function addtr(Request $request)
    {   
        
        $trberat = $request->TRBERAT;        
        
        $pktharga = DB::select('select PKTHARGA from paket where PKTID = ?', array($request->PKTID));
        
        foreach ($pktharga as $data)
        {
            $hrg = $data->PKTHARGA;  
            $hrgttl = $trberat*$hrg;                                                          

            DB::table('transaksi')->insert([
                [
                    'CUSTID' => $request->CUSTID,
                    'TRTGL' => $request->TRTGL,
                    'PKTID' => $request->PKTID,
                    'TRBERAT' => $trberat,
                    'TRTOTAL' => $hrgttl,
                ],
            ]);
        }
                        
        return redirect()->route('index');
    }

    public function qr($id)
    {                  
        $query = DB::table('transaksi as t')
                ->leftJoin('customer as c','c.CUSTID','=','t.CUSTID')
                ->leftJoin('paket as p','p.PKTID','=','t.PKTID')
                ->where('t.TRID', $id)
                ->get(); 

        $qr['id'] = $query[0]->TRID;
        $qr['tgl'] = $query[0]->TRTGL;
        $qr['nama'] = $query[0]->CUSTNAMA;
        $qr['data'] = $query[0]->TRID
                        .' / '.$query[0]->CUSTNAMA
                        .' / '.$query[0]->CUSTHP
                        .' / '.$query[0]->PKTNAMA
                        .' / '.$query[0]->TRBERAT
                        .' kg / Rp '.$query[0]->TRTOTAL
                        .' / '.$query[0]->TRTGL;
        
        return view('qr', $qr);
    } 

    public function pdf($id)
    {
        $qr = DB::table("transaksi as t")
            ->select('t.TRID', 't.CUSTID', 't.TRTGL', 't.PKTID', 't.TRBERAT', 't.TRTOTAL',
                    'c.CUSTNAMA as namacust', 'p.PKTNAMA as namapaket')
            ->join('customer as c', 't.CUSTID', '=', 'c.CUSTID')
            ->join('paket as p', 't.PKTID', '=', 'p.PKTID') 
            ->where('t.TRID', $id)           
            ->get();

        $pdf = PDF::loadview('pdf', ['qr'=> $qr]);
        return $pdf->stream('print-pdf-qr-laundry');
    }
}
