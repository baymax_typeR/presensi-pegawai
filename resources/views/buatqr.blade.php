@extends('layouts.main')

@section('title', 'Manager | QR Code')

@section('content')

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Pegawai
            <a class="btn btn-primary btn-icon-split scanner" style="float: right;">
                <span class="icon text-white-50">
                    <i class="fas fa-camera"></i>
                </span>
                <span class="text">Scan QR</span>
            </a>
        </h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">        
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Kota</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No.</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Kota</th>
                        <th>Aksi</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($pegawai as $data)
                    <tr>
                        <td style="width: 5px;">{{ $loop->iteration }}</td>
                        <td style="width: 15%;">{{ $data->PEGNIK }}</td>                        
                        <td>{{ $data->PEGNAMA }}</td>                        
                        <td>{{ $data->PEGKOTA }}</td>                                             
                        <td style="width: 28%; text-align: center">
                            <a type="button" href="{{ url('qr-code') }}/{{ $data->PEGNIK }}"
                                class="btn btn-success btn-icon-split" title="Buat QR Code">
                                <span class="icon text-white-50">
                                    <i class="fas fa-qrcode"></i>
                                </span>
                                <span class="text">Generate</span>
                            </a>                            
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-scan" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">            
            <form>                
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Scan QR</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span id="ef5" aria-hidden="true">×</span>
                    </button>
                </div>                
                <div class="modal-body mr-4 ml-4">
                    <div class="col-md-12" id="div-select" style="display: none">
                        <div class="form-group form-group-default">
                            <label class="form-control-label mb-0">Kamera</label>
                            <select class="form-control" id="pilih-camera"></select>
                        </div>
                    </div>
                    <div class="col-md-12">                        
                        <video id="preview" style="width: 100%; height: 250; margin: 0px auto;"></video>                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btnF5" data-dismiss="modal">Kembali</button>
                </div>
            </form>            
        </div>
    </div>
</div>

<div class="modal fade" id="pr-dtl" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">            
            <form id="formPresensi" name="formPresensi">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Detail Presensi</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span id="ef5" aria-hidden="true">×</span>
                    </button>
                </div>  
                <input id="action" hidden type="text" name="action" class="form-control" value="">              
                <div class="modal-body mr-4 ml-4">                    
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="PEGNIK">NIK</label>                        
                        <input id="PEGNIK" name="PEGNIK" type="text" class="form-control">
                    </div>
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="PEGNAMA">Nama</label>
                        <input id="PEGNAMA" name="PEGNAMA" type="text" class="form-control">
                    </div>
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="PEGKOTA">Kota</label>
                        <input id="PEGKOTA" name="PEGKOTA" type="text" class="form-control">
                    </div>
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="PRESTGL">Tanggal</label>
                        <input id="PRESTGL" name="PRESTGL" type="text" class="form-control">
                    </div>
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="PRESJAM">Jam</label>
                        <input id="PRESJAM" name="PRESJAM" type="text" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btnF5" data-dismiss="modal">Kembali</button>
                    <button id="submit" type="button" class="btn btn-primary">Simpan</button>
                </div>
            </form>            
        </div>
    </div>
</div>

<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

<script>
    var scanner = new Instascan.Scanner({
        video: document.getElementById('preview'),
        scanPeriod: 5,
        mirror: false,        
    });

    function animasi_leave(this_) {
        $(this_).css({
            "border": "solid",
            "border-color": "#3e8ef7"
        });
    }

    function animasi_enter(this_) {
        $(this_).css({
            "border": "",
            "border-color": ""
        });
    }

    $(".scanner").click(function () {
        scannerInstanscan();
    })

    function scannerInstanscan() {
        Instascan.Camera.getCameras().then(function (cameras) {
            if (cameras.length > 0) {
                // console.log("siji");
                if (cameras.length > 1) {
                    // console.log("loro");
                    $("#div-select").show();                    
                    $("#pilih-camera").html("");                
                    for (i in cameras) {
                        $("#pilih-camera").append("<option value='" + [i] + "'>" + cameras[i].name + "</option>");
                    }

                    if (cameras[1] != "") {
                        // console.log("telu");
                        scanner.start(cameras[1]);
                        $("#pilih-camera").val(1).change();
                    } else {
                        alert('No cameras found.');
                    }
                } else {
                    if (cameras[0] != "") {
                        scanner.start(cameras[0]);
                        $("#pilih-camera").val(0).change();
                    } else {
                        alert('No cameras found.');
                    }
                }
                $("#pilih-camera").change(function () {                    
                    var id = $("#pilih-camera :selected").val();
                    if (id != '') {
                        if (cameras[id] != "") {
                            scanner.start(cameras[id]);
                        } else {
                            alert('No cameras found.');
                        }
                    }
                })
            } else {
                console.error('No cameras found.');
                alert('No cameras found.');
            }
        }).catch(function (e) {
            console.error(e);
            alert(e);
        });

        $("#modal-scan").modal("show");
    }

    scanner.addListener('scan', function (content) {
        if (content != '') {
            // alert('papat');
            $("#modal-scan").modal("hide");

            var item = content.split("/");
            
            $('#PEGNIK').val(item[0]);
            $('#PEGNAMA').val(item[1]);
            $('#PEGKOTA').val(item[2]);
            $('#PRESTGL').val(item[3]);
            $('#PRESJAM').val(item[4]);            
            $("#pr-dtl").modal("show");

            console.log(item);            
        } else {
            var item = content.split("/");
            
            console.log(item);

            setTimeout(function () {
                swal({
                    title: "Data error!",
                    text: "Error.",
                    type: "error"
                }, function () {
                    $("#modal-scan").modal("hide");
                }, 3000);
            })
        }
    });    

    $('#submit').click(function (e) {
        e.preventDefault();
        $(this).html('Sending...');
        $.ajax({
            data: $('#formPresensi').serialize(),
            url: "{{ route('tbpres') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formPegawai').trigger('reset');
                $('#submit').html('Simpan');
                $('#pr-dtl').modal('hide');
                console.log('Sukses: ', data);                   
                
                Swal.fire({
                    title: 'Success!',
                    text: 'Your data has been updated.',
                    type: 'success',
                    showConfirmButton: true,                    
                }).then((result) => {
                    if (result.value) {
                        location.reload(true);
                    } else {
                        location.reload(true);
                    }
                });
            },
            error: function (data) {
                console.log('Error: ', data);                
            }
        });        
    });

    $('#modal-scan').on('hidden.bs.modal', function () {
        scanner.stop();
    })

    $('#pr-dtl').on('hidden.bs.modal', function () {
        $('#PEGNIK').text('');
        $('#PEGNAMA').text('');
        $('#PEGKOTA').text('');
        $('#PRESTGL').text('');
        $('#PRESJAM').text('');
    })     
</script>

@endsection