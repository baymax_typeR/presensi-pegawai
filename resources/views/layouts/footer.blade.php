<footer class="sticky-footer" style="background-color: #85B3CC;">
    <div class="container my-auto">
        <div class="copyright text-white text-center my-auto">
            <span><a data-toggle="modal" href="#modalJudul">Copyright</a> &copy; <a data-toggle="modal"
                    href="#modalMade">IsyanaWikramaDT</a> 2020 | with <a
                    href="https://startbootstrap.com/theme/sb-admin-2" target='_blank'>SB Admin 2</a></span>
        </div>
    </div>
</footer>

<div class="modal fade" id="modalMade" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="formPresensi" name="formPresensi">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="#">Made by</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <input id="action" hidden type="text" name="action" class="form-control" value="">
                <div class="modal-body mr-4 ml-4">
                    <ul><b>Isyana Wikrama D. T.</b> (1931733118) - 3E</ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalJudul" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <h5 class="modal-title" id="#">Judul</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <input id="action" hidden type="text" name="action" class="form-control" value="">
                <div class="modal-body mr-4 ml-4">
                    <ul><b>SI Presensi Pegawai</b> - pengguna Manajer</ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                </div>
            </form>
        </div>
    </div>
</div>