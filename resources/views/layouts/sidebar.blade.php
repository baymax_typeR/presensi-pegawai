<ul class="navbar-nav text-white sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: #85B3CC;">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-0">
            <i class="fas fa-users"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Presensi</div>
    </a>    

    <!-- Divider -->
    <hr class="sidebar-divider my-0" style="border-color: #ffffff;">

    <!-- Heading -->
    <div class="sidebar-heading text-white">
        Menu
    </div>    

    <!-- Pegawai -->
    <li class="nav-item {{ (request()->is('*pegawai*')) ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-user"></i>
            <span>Pegawai</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Interface</h6>                
                <a class="collapse-item" href="{{ route('pegawai-index') }}">Lihat Pegawai</a>
            </div>
        </div>
    </li>

    <!-- Presensi -->
    <li class="nav-item {{ (request()->is('*qr*')) ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fas fa-fw fa-qrcode"></i>
            <span>Presensi</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Interface</h6>
                <a class="collapse-item" href="{{ route('qr-index') }}">Mulai Presensi</a>                                              
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider" style="border-color: #ffffff;">    

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>    

</ul>