<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">

    <title>Laundry | PDF<title>

            <!-- Custom styles for this template-->
            <link href="{{asset('assets/css/sb-admin-2.min.css')}}" rel="stylesheet">

</head>

<body>

    <div class="card">
        <div class="card-body">
            <br>
            <h1 class="h1 text-center" style="color: #000000;">LAUNDRY</h1>
            <hr style="border: 2px solid #000000;">
            <br>
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6" style="border: 3px solid #000000; height: 700px;">
                    <div class="p-5">
                        <div class="text-center">
                            @foreach($qr as $data)
                            <h1 class="h2 text-gray-900 mb-3">Transaksi ID: {{$data->TRID}}</h1>
                            <h3 class="h4 text-gray-900 mb-3">{{ tgl_full($data->TRTGL, 1) }}</h3>
                            <h1 class="h4 text-gray-900 mb-3">Customer: {{$data->namacust}}</h1>

                            <br><br><br>
                            <!-- {!! QrCode::size(150)->generate($data->TRID); !!} -->
                            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(150)->generate($data->TRID)) !!} ">

                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </div>

    <br><br>

    <footer class="sticky-footer" style="background-color: #FFFFFF;">
        <div class="container my-auto">
            <div class="copyright text-center my-auto" style="color: #000000;">
                <span>Copyright &copy; IsyanaWikramaDT 2020</span>
            </div>
        </div>
    </footer>

</body>

</html>