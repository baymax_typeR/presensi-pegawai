@extends('layouts.main')

@section('title', 'Manager | Data Pegawai')

@section('content')

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Pegawai
            <a data-target="#tambahData" data-toggle="modal" class="btn btn-primary btn-icon-split tbpeg"
                style="float: right;" onclick="enableDisable()">
                <span class="icon text-white-50">
                    <i class="fas fa-user-plus"></i>
                </span>
                <span class="text">Tambah Data</span>
            </a>            
        </h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">        
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Kota</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No.</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Kota</th>
                        <th>Aksi</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($pegawai as $data)
                    <tr>
                        <td style="width: 5px;">{{ $loop->iteration }}</td>
                        <td style="width: 15%;">{{ $data->PEGNIK }}</td>                        
                        <td>{{ $data->PEGNAMA }}</td>                        
                        <td>{{ $data->PEGKOTA }}</td>                                             
                        <td style="width: 28%; text-align: center">
                            <a data-pegnik="{{ $data->PEGNIK }}" data-pegnama="{{ $data->PEGNAMA }}"
                                data-pegkota="{{ $data->PEGKOTA }}" class="btn btn-success edpeg"
                                title="Ubah Data Pegawai"><i class="fas fa-user-edit"></i>
                            </a>
                            <a data-pegnik="{{ $data->PEGNIK }}" class="btn btn-danger dlpeg"
                                title="Hapus Data Pegawai"><i class="fas fa-user-minus"></i>
                            </a>
                            <a type="button" href="{{ url('pr-pegawai/index') }}/{{ $data->PEGNIK }}" class="btn btn-secondary" title="Data Presensi">
                                <i class="fas fa-user-clock"></i>
                            </a>
                        </td>                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="tambahData" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">            
            <form id="formPegawai" name="formPegawai">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel"></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span id="ef5" aria-hidden="true">×</span>
                    </button>
                </div>
                <input id="action" hidden type="text" name="action" class="form-control" value="">
                <div class="modal-body mr-4 ml-4">                    
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="PEGNIK">NIK</label>
                        <input id="PEGNIK" name="PEGNIK" type="text" class="form-control"
                            placeholder="Contoh: 350506***" onkeyup="enableDisable()">
                    </div>
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="PEGNAMA">Nama</label>
                        <input id="PEGNAMA" name="PEGNAMA" type="text" class="form-control"
                            placeholder="Contoh: Anselma P" onkeyup="enableDisable()">
                    </div>
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="PEGKOTA">Kota</label>
                        <input id="PEGKOTA" name="PEGKOTA" type="text" class="form-control"
                            placeholder="Contoh: Bandung" onkeyup="enableDisable()">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btnF5" data-dismiss="modal">Kembali</button>
                    <button id="submit" type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>            
        </div>
    </div>
</div>

<script>
    var submitBtn = document.getElementById('submit');

    function enableDisable() {        
        var a = document.getElementById('PEGNIK').value;
        var b = document.getElementById('PEGNAMA').value;
        var c = document.getElementById('PEGKOTA').value;

        if (a && b && c) {
            submitBtn.disabled = false;            
        } else {
            submitBtn.disabled = true;
        }
    }    
</script>

<script>
    $('.tbpeg').click(function() {
        $('#staticBackdropLabel').html('Tambah Data Pegawai');
        $('#action').val('tambah');
        $('#tambahData').modal({
            show: true
        })
    });

    $('.edpeg').click(function() {
        $('#staticBackdropLabel').html('Ubah Data Pegawai');        
        $('#action').val('edit');
        $('#PEGNIK').val($(this).data('pegnik'))
        $('#PEGNAMA').val($(this).data('pegnama'))
        $('#PEGKOTA').val($(this).data('pegkota'))
        $('#tambahData').modal({
            show: true
        })
    });

    $('#submit').click(function (e) {
        e.preventDefault();
        $(this).html('Sending...');
        $.ajax({
            data: $('#formPegawai').serialize(),
            url: "{{ route('tbpeg') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formPegawai').trigger('reset');
                $('#submit').html('Simpan');
                $('#tambahData').modal('hide');
                console.log('Sukses: ', data);                   
                
                Swal.fire({
                    title: 'Success!',
                    text: 'Your data has been updated.',
                    type: 'success',
                    showConfirmButton: true,                    
                }).then((result) => {
                    if (result.value) {
                        location.reload(true);
                    } else {
                        location.reload(true);
                    }
                });
            },
            error: function (data) {
                console.log('Error: ', data);                
            }
        });
    });

    $(document).on('click', '.dlpeg', function (e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#E74A3B',
            cancelButtonColor: '#1CC88A',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {

                var id = $(this).data('pegnik');

                $.ajax({
                    url: "{{ url('/pegawai/delete') }}/" + id,
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
                        console.log('Sukses: ', data);                          
                    },
                    error: function (data) {
                        console.log('Error: ', data);
                    }
                });            

                Swal.fire({
                    title: 'Deleted!',
                    text: 'Your data has been deleted.',
                    type: 'success',
                    showConfirmButton: true,
                }).then((result) => {
                    if (result.value) {
                        location.reload(true);
                    } else {
                        location.reload(true);
                    }
                });              
            } else {
                swal.close();
            }
        });
    });
</script>

@endsection