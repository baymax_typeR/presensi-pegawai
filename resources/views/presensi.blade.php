@extends('layouts.main')

@section('title', 'Manager | Data Presensi')

@section('content')

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Presensi</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID Presensi</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Tanggal</th>
                        <th>Jam</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID Presensi</th>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Tanggal</th>
                        <th>Jam</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($presensi as $data)
                    <tr>
                        <td>PRES{{ $data->PRESID }}</td>
                        <td style="width: 15%;">{{ $data->PEGNIK }}</td>                        
                        <td>{{ $data->PEGNAMA }}</td>                        
                        <td>{{ tgl_full($data->PRESTGL, 1) }}</td>                                             
                        <td>{{ $data->PRESJAM }}</td>                                                                                             
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection