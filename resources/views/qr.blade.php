@extends('layouts.main')

@section('title', 'Manager | QR Code')

@section('content')

<!-- Outer Row -->
<div class="card o-hidden border-0">
    <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="p-5">
                    
                    <div class="text-center">
                        <h1 class="h3 text-gray-900 mb-3">{{ tgl_full(now(), 2) }}</h1>
                        <h1 class="h4 text-gray-900 mb-3">NIK: {{ $nik }}</h1>
                        <h3 class="h4 text-gray-900 mb-3">{{ $nama }}</h3>
                        <h1 class="h4 text-gray-900 mb-3">{{ $kota }}</h1>
                        
                        {!! QrCode::size(150)->generate($data); !!}                        

                    </div>

                    <br><br>

                    <div class="text-center">
                        <a type="button" href="{{ route('qr-index') }}" class="btn btn-secondary btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-backward"></i>
                            </span>
                            <span class="text">Kembali</span>
                        </a>
                    </div>

                </div>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

@endsection