@extends('layouts.main')

@section('title', 'Laundry | Transaksi')

@section('content')

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Transaksi
            <a href="#tambahData" data-toggle="modal" class="btn btn-primary btn-icon-split" style="float: right;">
                <span class="icon text-white-50">
                    <i class="fa fa-plus"></i>
                </span>
                <span class="text">Tambah Data</span>
            </a>
        </h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>TRID</th>
                        <th>Customer</th>
                        <th>Tanggal</th>
                        <th>Paket</th>
                        <th>Berat</th>
                        <th>Total</th>
                        <th>Cetak QR</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>TRID</th>
                        <th>Customer</th>
                        <th>Tanggal</th>
                        <th>Paket</th>
                        <th>Berat</th>
                        <th>Total</th>
                        <th>Cetak QR</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($transaksi as $data)
                    <tr>
                        <td>{{ $data->TRID }}</td>
                        <td>{{ $data->namacust }}</td>
                        <td>{{ tgl_full($data->TRTGL, 1) }}</td>                        
                        <td>{{ $data->namapaket }}</td>
                        <td>{{ $data->TRBERAT }} kg</td>
                        <td>Rp {{ $data->TRTOTAL }}</td>
                        <td style="text-align: center">
                            <a type="button" href="{{ url('/transaksi/qr-code') }}/{{ $data->TRID }}" class="btn btn-primary">
                                <i class="fas fa-print"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- <div class="modal fade" id="tambahData" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ route('addtr') }}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Tambah Data Transaksi</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span id="ef5" aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body mr-4 ml-4">
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="CUSTID">Customer</label>
                        <select id="CUSTID" class="form-control" name="CUSTID">
                            <option disabled selected>--- Pilih Customer ---</option>
                            @foreach($customer as $list)
                            <option value="{{$list->CUSTID}}"> {{$list->CUSTNAMA}} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="TRTGL">Tanggal</label>
                        <input id="TRTGL" name="TRTGL" type="date" class="form-control" placeholder="Tanggal transaksi">
                    </div>
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="PKTID">Paket</label>
                        <select id="PKTID" class="form-control" name="PKTID">
                            <option disabled selected>--- Pilih Paket ---</option>
                            @foreach($paket as $list)
                            <option value="{{$list->PKTID}}"> {{$list->PKTNAMA}} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group form-group-default">
                        <label class="form-control-label mb-0" for="TRBERAT">Berat</label>
                        <input id="TRBERAT" name="TRBERAT" type="number" class="form-control"
                            placeholder="Berat transaksi">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btnF5" data-dismiss="modal">Kembali</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div> -->

@endsection