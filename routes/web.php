<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\pegawaiController;
use App\Http\Controllers\presensiController;
use App\Http\Controllers\qrController;

Route::get('/pegawai/index', [pegawaiController::class, 'index'])->name('pegawai-index');
Route::post('/pegawai/tambah', [pegawaiController::class, 'tbpeg'])->name('tbpeg');
Route::get('/pegawai/delete/{id}', [pegawaiController::class, 'dlpeg']);

Route::get('/pr-pegawai/index/{id}', [presensiController::class, 'index']);
Route::post('/pr-pegawai/tambah', [presensiController::class, 'tbpres'])->name('tbpres');

Route::get('/qr-code/index', [qrController::class, 'index'])->name('qr-index');
Route::get('/qr-code/{id}', [qrController::class, 'qr']);

// Route::get('/pegawai/edit/{id}', [pegawaiController::class, 'edpeg'])->name('edpeg');
// Route::get('/transaksi/qr-code/{id}', [transaksiController::class, 'qr']);
// Route::get('/transaksi/pdf/{id}', [transaksiController::class, 'pdf']);

Route::get('/welcome', function () {
    return view('welcome');
});
